## task

to coordinate two treads ordinal access for printing their value.

## missed part

Test is not implemented `package org.vlasiuk.solution.service.OrdinalPrintServiceTest#print`. (it could be ignored temporary)
It was not asked in task, but would be good to have one.

Test implementation takes a little bigger time than in usual way, because it needs to have implemented additional util functionality (can be used in all tests in future).
Proposed steps:

    - logger can not be mocked without reflection because it is private and final;
    - it needs to replace appender;
    - alternative appender could have captor mechanizm;
    - in test directly we would have a chance to check captored values.
