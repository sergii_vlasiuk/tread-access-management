package org.vlasiuk.solution.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrdinalPrintService implements PrintService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrdinalPrintService.class);

    @Override
    public synchronized void print(Integer intValue) {
        try {
            LOGGER.info("Value: {}", intValue);
            notify();
            wait(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
