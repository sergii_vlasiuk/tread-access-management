package org.vlasiuk.solution.service.thread;

import org.vlasiuk.solution.service.PrintService;

public class DigitTreadService implements Runnable {
    private final Integer value;
    private final PrintService printService;

    public DigitTreadService(Integer value, PrintService printService) {
        this.value = value;
        this.printService = printService;
    }

    @Override
    public void run() {
        do {
            pushPrint();
        } while (true);
    }

    void pushPrint() {
        printService.print(value);
    }

}
