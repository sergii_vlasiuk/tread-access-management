package org.vlasiuk.solution;

import org.vlasiuk.solution.service.OrdinalPrintService;
import org.vlasiuk.solution.service.PrintService;
import org.vlasiuk.solution.service.thread.DigitTreadService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        PrintService printService = new OrdinalPrintService();

        executorService.submit(new DigitTreadService(0, printService));
        executorService.submit(new DigitTreadService(1, printService));
    }
}
