package org.vlasiuk.solution.service;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrdinalPrintServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrdinalPrintServiceTest.class);

    @Test
    public void print() {
        LOGGER.error("It needs more to manage the test with mockito.");
        throw new UnsupportedOperationException("It needs more to manage the test with mockito.");
    }
}