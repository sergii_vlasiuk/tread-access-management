package org.vlasiuk.solution.service.thread;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.vlasiuk.solution.service.PrintService;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DigitTreadServiceTest {
    private DigitTreadService testee;
    @Captor
    private ArgumentCaptor<Integer> valueCaptor;
    @Mock
    private PrintService ordinalServiceMock;
    private int randomValue = new Random().nextInt();

    @Before
    public void setUp() {
        testee = new DigitTreadService(randomValue, ordinalServiceMock);
    }

    @Test(expected = Exception.class)
    public void run() {
        doThrow(new Exception()).when(ordinalServiceMock).print(any());

        testee.run();
    }

    @Test
    public void pushPrint() {
        testee.pushPrint();

        verify(ordinalServiceMock).print(valueCaptor.capture());
        assertThat(valueCaptor.getValue()).isEqualTo(randomValue);
    }
}